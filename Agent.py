#YourAgentfor solving Raven's Progressive Matrices. You MUST modify this file.
#
# You may also create and submit new files in addition to modifying this file.
#
# Make sure your file retains methods with the signatures:
# def __init__(self)
# def Solve(self,problem)
#
# These methods will be necessary for the project's main method to run.

# Install Pillow and uncomment this line to access image processing.
from PIL import Image, ImageChops
from RavensImage import RavensImage
import logging
from RavensTransform import RavensTransform
import traceback
# Setup logging

# logging.basicConfig(level=logging.DEBUG,
#                     format='-%(levelname)s : %(message)s')

CONFIDENT = 0.99
class Agent:
    # The default constructor for your Agent. Make sure to execute any
    # processing necessary before your Agent starts solving problems here.
    #
    # Do not add any variables to this signature; they will not be used by
    # main().
    def __init__(self):
        self._lst_identity = ["identity", "identityFlipByX", "identityFlipByY"]
        pass

    # The primary method for solving incoming Raven's Progressive Matrices.
    # For each problem, your Agent's Solve() method will be called. At the
    # conclusion of Solve(), your Agent should return an int representing its
    # answer to the question: 1, 2, 3, 4, 5, or 6. Strings of these ints
    # are also the Names of the individual RavensFigures, obtained through
    # RavensFigure.getName(). Return a negative number to skip a problem.
    #
    # Make sure to return your answer *as an integer* at the end of Solve().
    # Returning your answer as a string may cause your program to crash.
    def Solve(self, problem):
        try:
            if 'Basic Problem B' in problem.name:
                return -1
            if 'Basic Problem C' in problem.name:
                return -1
            if 'Challenge' in problem.name:
                return -1
            _lst_answer = [problem.figures['1'], problem.figures['2'], problem.figures['3'],
                           problem.figures['4'], problem.figures['5'], problem.figures['6'], problem.figures['7'], problem.figures['8']]
            #_ravenTransform = RavensTransform()
            print '---- Solving problem: {} ----- '.format(problem.name)
            _lst_transform = []
            _image_A, _image_B, _image_C = self.openImage(problem.figures['A'], problem.figures['B'], problem.figures['C'])
            _image_D, _image_E, _image_F = self.openImage(problem.figures['D'], problem.figures['E'], problem.figures['F'])
            _G = problem.figures['G']
            _image_G = Image.open(_G.visualFilename).convert('L')
            _H = problem.figures['H']
            _image_H = Image.open(_H.visualFilename).convert('L')
            _lst_answer_img = [Image.open(x.visualFilename).convert('L') for x in _lst_answer]
            _ravenTransform = RavensTransform(_lst_answer_img, _image_A, _image_B, _image_C, _image_D, _image_E, _image_F, _image_G, _image_H)
            answer = -1
            if 'D' in problem.name:
                answer = _ravenTransform.transform()
            if 'E' in problem.name: 
                answer = _ravenTransform.binaryTransform()
            return answer if answer is not None else -1
            print '---- End ----- '
        except Exception as e:
            logging.error("@@@@ EXCEPTION: {} @@@@@".format(e))
            traceback.print_exc()
            return -1

    def openImage(self, figure_1, figure_2, figure_3):
        _image_A = Image.open(figure_1.visualFilename).convert('L')
        _image_B = Image.open(figure_2.visualFilename).convert('L')
        _image_C = Image.open(figure_3.visualFilename).convert('L')
        return _image_A, _image_B, _image_C

