import os
import errno
import itertools
import requests
from PIL import Image, ImageChops
from numpy import average, linalg, dot
from PIL.Image import FLIP_LEFT_RIGHT, FLIP_TOP_BOTTOM
import numpy as np

# This class used for providing images API to support the SM class

class RavensImage:
    def __init__(self, _lst_answer, _image_A, _image_B,_image_C,_image_D,_image_E,_image_F, _image_G, _image_H):
        self._Image = None
        self._lst_answer_img = _lst_answer
        self._image_A = _image_A
        self._image_B = _image_B
        self._image_C = _image_C
        self._image_D = _image_D
        self._image_E = _image_E
        self._image_F = _image_F
        self._image_G = _image_G
        self._image_H = _image_H


    def  getRowOne(self):
        return [self._image_A, self._image_B, self._image_C]
    def  getRowTwo(self):
        return [self._image_D, self._image_E, self._image_F]
    def  getRowThree(self):
        return [self._image_G, self._image_H]
    def getColOne(self):
        return [self._image_A, self._image_D, self._image_G]
    def getColTwo(self):
        return [self._image_B, self._image_E, self._image_H]
    def getColThree(self):
        return [self._image_C, self._image_F]
    def getImageA(self):
        return self._image_A
    def getImageB(self):
        return self._image_B
    def getImageC(self):
        return self._image_C
    def getImageD(self):
        return self._image_D
    def getImageE(self):
        return self._image_E
    def getImageF(self):
        return self._image_F
    def getImageG(self):
        return self._image_G
    def getImageH(self):
        return self._image_H
    def getListAnswerImg(self):
        return self._lst_answer_img
    #Return Images object
    def getImage(self):
        return self._Image

    #Set image
    #para: string, imageURL
    def setImage(self, _imgSource):
        self._Image = _imgSource

    def convertTothumbnail(self, _image):
        _size=(128,128)
        _image = _image.resize(_size, Image.ANTIALIAS)
        return _image

    def getIdentity(self, _img):
        return _img

    def rotateImage90(self, img):
        return img.rotate(-90)
    def rotateImage180(self, img):
        return img.rotate(-180)
    def rotateImage270(self, img):
        return img.rotate(-270)


    def getIdentityFlipByAxisY(self, img):
        return img.transpose(FLIP_LEFT_RIGHT)


    def getIdentityFlipByAxisX(self, img):
        return img.transpose(FLIP_TOP_BOTTOM)

    def rotateImage90FlipByAxisX(self, img):
        _getImage = self.rotateImage90(img)
        return self.getIdentityFlipByAxisX(_getImage)

    def rotateImage90FlipByAxisY(self, img):
        _getImage = self.rotateImage90(img)
        return self.getIdentityFlipByAxisY(_getImage)

    def rotateImage180FlipByAxisX(self, img):
        _getImage = self.rotateImage180(img)
        return self.getIdentityFlipByAxisX(_getImage)

    def rotateImage180FlipByAxisY(self, img):
        _getImage = self.rotateImage180(img)
        return self.getIdentityFlipByAxisY(_getImage)

    def rotateImage270FlipByAxisX(self, img):
        _getImage = self.rotateImage270(img)
        return self.getIdentityFlipByAxisX(_getImage)

    def rotateImage270FlipByAxisY(self, img):
        _getImage = self.rotateImage270(img)
        return self.getIdentityFlipByAxisY(_getImage)

    def getImageSimilarity(self, _imgA, _imgB):
        _images = [self.convertTothumbnail(_imgA), self.convertTothumbnail(_imgB)]
        _vectors = []
        _norms = []
        _vectors, _norms = self. getNormsOfTwoImages(_images)
        _imgA, _imgB = _vectors
        _imgA_norm, _imgB_norm = _norms
        try:
            if _imgA_norm == 0 or _imgB_norm == 0:
                return 0
            _res = dot(_imgA / _imgA_norm, _imgB / _imgB_norm)
            if np.isnan(_res):
                return 0
            return _res
        except ValueError as err:
            print '@@ EXCEPTION @@: {}'.format(err)
            return 0

    def getDifference(self, _imgA, _imgB):
        return ImageChops.difference(_imgA, _imgB)

    @staticmethod
    def getBlackPixels(_img):
        _img = _img.convert('L')
        _pixels = _img.getdata() 
        _range = 50
        _nblack = 0
        for _pixel in _pixels:
            if _pixel < _range:
                _nblack += 1
        return _nblack

    def getNormsOfTwoImages(self, _images):
        _vectors = []
        _norms = []
        for _image in _images:
            _vector = []
            for _pixel in _image.getdata():
                _vector.append(average(_pixel))
            _vectors.append(_vector)
            _norms.append(linalg.norm(_vector, 2))
        return _vectors, _norms

    @staticmethod
    def getTotalPixels(_img):
        _img = _img.convert('L')
        _pixels = _img.getdata()  
        _number_pixels = len(_pixels)
        return _number_pixels

     # Not sure add is intersect
    def getIntersect(self, _imgA, _imgB):
        _img = ImageChops.add(_imgA, _imgB)
        return _img

    def getSubtract(self, _imgA, _imgB):
        _img = ImageChops.subtract(_imgA, _imgB)
        return _img

    # get the share common and then invert
    def getLogicalXOR(self, _imgA, _imgB):
        _imgA = _imgA.convert('1')
        _imgB = _imgB.convert('1')
        return ImageChops.logical_xor(_imgA, _imgB)

    # means show the part which exist in two images
    def getLogicalOR(self, _imgA, _imgB):
        _imgA = _imgA.convert('1')
        _imgB = _imgB.convert('1')
        return ImageChops.logical_or(_imgA, _imgB)

    # means and two images ( union)
    def getLogicalAND(self, _imgA, _imgB):
        _imgA = _imgA.convert('1')
        _imgB = _imgB.convert('1')
        return ImageChops.logical_and(_imgA, _imgB)

    def getUnion(self, _imgA, _imgB):
        _imgA = ImageChops.invert(_imgA)
        _imgB = ImageChops.invert(_imgB)
        _img_add = self.getIntersect(_imgA, _imgB)
        return ImageChops.invert(_img_add)

    # show the part in _imgA  but not in _imgB
    def getRealSubtraction(self, _imgA, _imgB):
        _img = self.getLogicalXOR(_imgA, _imgB)
        return ImageChops.invert(_img)

    def getEuclideanDistance(self, _imgA, _imgB):
        _imgA = np.asarray(_imgA.convert('1'))
        _imgB = np.asarray(_imgB.convert('1'))
        return np.linalg.norm(_imgA - _imgB)

    def getSimilarityAlternate(self, _imgA, _imgB):
        _similarity =  float(self.getBlackPixels(self.getIntersect(_imgA, _imgB))) / float(self.getBlackPixels(self.getUnion(_imgA, _imgB))) 
    