from RavensImage import RavensImage
import operator
import traceback
# logging.basicConfig(level=logging.DEBUG,
#                     format='-%(levelname)s : %(message)s')
CONFIDENT_BINARY = 0.99
CONFIDENT = 0.98
class RavensTransform:
    def __init__(self, _lst_answer, _image_A, _image_B,_image_C,_image_D,_image_E,_image_F, _image_G, _image_H):
        self._ravensImage = RavensImage(_lst_answer, _image_A, _image_B,_image_C,_image_D,_image_E,_image_F, _image_G, _image_H)
        self._lst_identity = ["identity", "identityFlipByX", "identityFlipByY"]

    # Return a string : reflection, identity, rotation
    def getUnaryTransform(self, _source, _dest):
        #_affine_transform_source = self.getAffineTransformation(_source) # dictionary of affine transform
        _affine_score_dict = self.getAffineSimilarityScore(_source, _dest)
        _transform_type = self.getMaxSimilarity(_affine_score_dict) # get similarity score with the dest
        return _transform_type

    #Solve D-03
    def getTransformTotalPixelsByAdding(self, _row_1, _row_2 ):
        _transform_row_1= self.transformByTotalPixel(_row_1)
        _transform_row_2 = self.transformByTotalPixel(_row_2)
        if _transform_row_1 == _transform_row_2:
            _lst_answer = []
            _lst_answer_img = self._ravensImage.getListAnswerImg()
            _row_3 = self._ravensImage.getRowThree()
            for i in range(0, len(_lst_answer_img)):
                _row_3.append(_lst_answer_img[i])
                _transform_row_3 = self.transformByTotalPixel(_row_3)
                _lst_answer.append(_transform_row_3)
                _row_3 = self._ravensImage.getRowThree()
            _minIndex = self.getMinDistance(_transform_row_1,_lst_answer)
            print 'found answer: {}'.format(_minIndex + 1)
            return _minIndex + 1

    #solve D-04
    def getTransformTotalPixelsByDifference(self, _row_1, _row_2 ):
        _transform_row_1 = self.getTotalDifference(_row_1)
        _transform_row_2 = self.getTotalDifference(_row_2)
        if float(_transform_row_1) / float(_transform_row_2) >= 0.99:
            _lst_answer = []
            _lst_answer_img = self._ravensImage.getListAnswerImg()
            _row_3 = self._ravensImage.getRowThree()
            for i in range(0, len(_lst_answer_img)):
                _row_3.append(_lst_answer_img[i])
                _transform_row_3 = self.getTotalDifference(_row_3)
                _lst_answer.append(_transform_row_3)
                _row_3 = self._ravensImage.getRowThree()
            _minIndex = self.getMinDistance(_transform_row_1,_lst_answer)
            print 'found answer: {}'.format(_minIndex + 1)
            return _minIndex + 1

    def binaryTransform(self):
        # _answer = self.getBinaryTransformByUnion()
        # _answer = self.getBinaryTransformByUnionForCol()
        # _answer = self.getBinaryTransformByPixel()
        # return _answer
        try:
            return self.getBinaryTransformByUnion()
        except Exception as err:
            print ("@@@@ EXCEPTION: {} @@@@@".format(err))
            traceback.print_exc()
            return -1
    def transform(self):
        try:
            _lst_transform = []
            answer = -1
            _image_A, _image_B, _image_C = self._ravensImage.getImageA(), self._ravensImage.getImageB(),  self._ravensImage.getImageC()
            _image_D, _image_E, _image_F = self._ravensImage.getImageD(), self._ravensImage.getImageE(),  self._ravensImage.getImageF()
            _image_G, _image_H =  self._ravensImage.getImageG(), self._ravensImage.getImageH()
            _lst_answer_img = self._ravensImage.getListAnswerImg()
            # If affine works
            if self.getAffineSimilarityScore(self._ravensImage.getImageA(), self._ravensImage.getImageB()) is not None:
                _transform_type = self.applyAffineTransformtoRow(self._ravensImage.getImageA(),  self._ravensImage.getImageB(),  self._ravensImage.getImageC())
                _lst_transform.append(_transform_type)
                _transform_type = self.applyAffineTransformtoRow(self._ravensImage.getImageD(),  self._ravensImage.getImageE(),  self._ravensImage.getImageF())
                _lst_transform.append(_transform_type)
                if self._isSublist(_lst_transform, self._lst_identity):
                    _transform_H =  self.applyTransform('identity', self._ravensImage.getImageH())
                    for i in  range(0, len(_lst_answer_img)):
                        _transform_answer = self.applyTransform('identity', _lst_answer_img[i])
                        _confident_score =self.getSimilarityIdentity(_transform_H, _transform_answer)
                        if _confident_score >= CONFIDENT:
                            answer = i + 1
                            print 'Found answer: {}'.format(answer)
                            return answer
            else:
                answer = self.getTransformTotalPixelsByDifference(self._ravensImage.getRowOne(), self._ravensImage.getRowTwo())
                return answer if answer is not None else -1
            return answer
        except Exception as err:
            print ("@@@@ EXCEPTION: {} @@@@@".format(err))
            traceback.print_exc()
            return -1


    # tranform img with given transform type: rotation, reflection
    def applyTransform (self, _transform_type, _img):
        if _transform_type == 'identity':
            return self._ravensImage.getIdentity(_img)
        elif _transform_type == 'rotate90':
            return self._ravensImage.rotateImage90(_img)
        elif _transform_type == 'rotate180':
            return self._ravensImage.rotateImage180(_img)
        elif _transform_type == 'rotate270':
            return self._ravensImage.rotateImage270(_img)
        elif _transform_type == 'identityFlipByY':
            return self._ravensImage.getIdentityFlipByAxisY(_img)
        elif _transform_type == 'identityFlipByX':
            return self._ravensImage.getIdentityFlipByAxisX(_img)
        elif _transform_type == 'rotate90FlipByX':
            return self._ravensImage.rotateImage90FlipByAxisX(_img)
        elif _transform_type == 'rotate90FlipByY':
            return self._ravensImage.rotateImage90FlipByAxisY(_img)
        elif _transform_type == 'rotate180FlipByX':
            return self._ravensImage.rotateImage180FlipByAxisX(_img)
        elif _transform_type == 'rotate180FlipByY':
            return self._ravensImage.rotateImage270FlipByAxisX(_img)
        elif _transform_type == 'rotate180FlipByY':
            return self._ravensImage.rotateImage180FlipByAxisY(_img)
        elif _transform_type == 'rotate270FlipByX':
            return self._ravensImage.rotateImage270FlipByAxisX(_img)
        elif _transform_type == 'rotate270FlipByY':
            return self._ravensImage.rotateImage270FlipByAxisY(_img)

    # Get affine transform type for a row
    # Return String: reflection, rotation, etc..
    def applyAffineTransformtoRow(self, _image_A, _image_B, _image_C):
        _affine_type = self.getUnaryTransform(_image_A, _image_B)
        _img_transform = self.applyTransform(_affine_type, _image_B)
        score = self.getSimilarityIdentity(_img_transform, _image_C)
        if score >= CONFIDENT :
            return _affine_type
        else:
            return "UNKNOWN"
    def getAffineTransformation(self, _img):
        _dict = {}
        _dict['identity'] = self._ravensImage.getIdentity(_img)
        _dict['rotate90'] = self._ravensImage.rotateImage90(_img)
        _dict['rotate180'] = self._ravensImage.rotateImage180(_img)
        _dict['rotate270'] = self._ravensImage.rotateImage270(_img)
        _dict['identityFlipByY'] = self._ravensImage.getIdentityFlipByAxisY(_img)
        _dict['identityFlipByX'] = self._ravensImage.getIdentityFlipByAxisX(_img)
        _dict['rotate90FlipByX'] = self._ravensImage.rotateImage90FlipByAxisX(_img)
        _dict['rotate90FlipByY'] = self._ravensImage.rotateImage90FlipByAxisY(_img)
        _dict['rotate180FlipByX'] = self._ravensImage.rotateImage180FlipByAxisX(_img)
        _dict['rotate180FlipByY'] = self._ravensImage.rotateImage180FlipByAxisY(_img)
        _dict['rotate270FlipByX'] = self._ravensImage.rotateImage270FlipByAxisX(_img)
        _dict['rotate270FlipByY'] = self._ravensImage.rotateImage270FlipByAxisY(_img)
        return _dict

    # compare each AffineTransform with the image dest and get similarity score
    def getAffineSimilarityScore(self, _source, _dest):
        _dict_affine_transform = self.getAffineTransformation(_source)
        _dict_score = {}
        for key, value in _dict_affine_transform.iteritems():
            _similarity_score = self.getSimilarityIdentity(value, _dest)
            if _similarity_score >= CONFIDENT:
                _dict_score[key] = _similarity_score
        return _dict_score if _dict_score else None

    #Return a key which has value max: rotation, reflectiton
    def getMaxSimilarity(self, _dict):
        return max(_dict.iteritems(), key=operator.itemgetter(1))[0]


    def getSimilarityIdentity(self, _source, _dest):
        return self._ravensImage.getImageSimilarity(_source, _dest)

    def isHighConfience(self, _score):
        if 100 - _score < 10:
            return True
        else:
            return False

    def isDifferent(self, _img_A, _img_B):
        return False if self.getSimilarityIdentity(_img_A, _img_B) >= CONFIDENT else True

    def getTotalPixel(self, _row):
        _dark_pixel = 0
        for i in _row:
            _dark_pixel += self._ravensImage.getBlackPixels(i)
        return _dark_pixel

    def isAllDifferent(self, _row):
        if self.isDifferent(_row[0], _row[1]) and self.isDifferent(_row[1], _row[2]): #and self.isDifferent(_row[0], _row[2]):
            return True
        else:
            return False

    def transformByTotalPixel(self, _row):
        if self.isAllDifferent(_row):
            return self.getTotalPixel(_row)
        else:
            return -1

    def getTotalDifference(self, _row):
        _diff_A_B = RavensImage.getBlackPixels(self._ravensImage.getDifference(_row[0], _row[1]))
        _diff_A_C = RavensImage.getBlackPixels(self._ravensImage.getDifference(_row[0], _row[2]))
        _diff_B_C = RavensImage.getBlackPixels(self._ravensImage.getDifference(_row[1], _row[2]))
        return _diff_A_B + _diff_A_C + _diff_B_C

    def _isSublist(self,_lst1, _lst2):
        _lst1 = [element for element in _lst1 if element in _lst2]
        _lst2 = [element for element in _lst2 if element in _lst1]
        return _lst1 == _lst2

    def getMinDistance(self, _compare_value, _lst_answer):
        _tmp = []
        for i in _lst_answer:
            _tmp.append(abs(i - _compare_value))
        _minValue = min(_tmp)
        return _tmp.index(_minValue)

    def getBinaryTransform(self, _img_A, _img_B):
        _dict = {}
        #for logical operator, remember to convert back to RGB or L mode
        _dict['union'] = self._ravensImage.getUnion(_img_A, _img_B)
        _dict['logicalOR'] = self._ravensImage.getLogicalOR(_img_A, _img_B)
        _dict['intersection'] = self._ravensImage.getIntersect(_img_A, _img_B)
        _dict['subtraction'] = self._ravensImage.getSubtract(_img_A, _img_B)
        _dict['back_subtraction'] = self._ravensImage.getSubtract(_img_B, _img_A)
        _dict['exclusive_OR'] = self._ravensImage.getLogicalXOR(_img_B, _img_A)
        _dict['difference'] = self._ravensImage.getRealSubtraction(_img_B, _img_A)
        return _dict


    def applyBinaryTransform(self,_transform_type, _imgA, _imgB):
        if _transform_type == 'union':
            return self._ravensImage.getLogicalAND(_imgA, _imgB)
        elif _transform_type == 'logicalOR':
            return self._ravensImage.getLogicalOR(_imgA, _imgB)
        elif _transform_type == 'intersection':
            return self._ravensImage.getIntersect(_imgA, _imgB)
        elif _transform_type == 'subtraction':
            return self._ravensImage.getSubtract(_imgA, _imgB)
        elif _transform_type == 'back_subtraction':
            return  self._ravensImage.getSubtract(_imgA, _imgB)
        elif _transform_type == 'exclusive_OR':
            return self._ravensImage.getLogicalXOR(_imgA, _imgB)
        elif _transform_type == 'difference':
            return self._ravensImage.getRealSubtraction(_imgA, _imgB)


    # compare each BinaryTransform with the image dest and get similarity score
    def getBinarySimilarityScore(self, _row):
        _dict_binary_transform = self.getBinaryTransform(_row[0], _row[1])
        _dict_score = {}
        for key, value in _dict_binary_transform.iteritems():
            _similarity_score = self.getSimilarityIdentity(value, _row[2])
            if _similarity_score == 0:
                return 0
            #print '==== DEBUG: key: {} with similarity value: {}'.format(key, _similarity_score)
            #print '==== DEBUG: key: {} with similarity value: {}'.format(key, _similarity_score)
            # if _similarity_score >= CONFIDENT_BINARY:
            #     _dict_score[key] = _similarity_score
            _dict_score[key] = _similarity_score
        return _dict_score if _dict_score else None

    # Get affine transform type for a row
    # Return String: logical: xor, union, etc..
    def getBinaryTransformType(self, _dict):
        _value =  self.getMaxSimilarity(_dict)
        if _value is not None:
            return _value
        else:
            print '@@@Error: Value of getBinaryTransformType is None @@@@'


    #Solve E-01 to E-03
    def getBinaryTransformByUnion(self):
        _lst_answer = []
        _rowOne = self._ravensImage.getRowOne()
        _rowTwo = self._ravensImage.getRowTwo()
        _lst_answer_img = self._ravensImage.getListAnswerImg()
        _transform_dict_row_one = self.getBinarySimilarityScore(_rowOne)
        _transform_dict_row_two = self.getBinarySimilarityScore(_rowTwo)
        if _transform_dict_row_one == 0 or _transform_dict_row_two == 0:
            print 'Switch to specific case'
            return self.getTransformBySubtractionA_B_equal_C()
        _transform_type_one = self.getBinaryTransformType(_transform_dict_row_one)
        _transform_type_two = self.getBinaryTransformType(_transform_dict_row_two)
        for i in  range(0, len(_lst_answer_img)):
            _row_3 = self._ravensImage.getRowThree()
            _row_3.append(_lst_answer_img[i])
            _img = self.applyBinaryTransform(_transform_type_one, _row_3[0], _row_3[1])
            if self.getSimilarityIdentity(_img,  _row_3[2]):
                _lst_answer.append(self.getSimilarityIdentity(_img,  _row_3[2]))
        _max_value = max(_lst_answer)
        _max_index = _lst_answer.index(_max_value)
        print 'Found answer: {}'.format(_max_index + 1)
        return _max_index + 1

        #  _min_value = min(_lst_answer)
        # _min_list = self.searchIndexWithValue(_min_value, _lst_answer) #[1,7]
        # _tmp_dict = {}
        # for i in _min_list:
        #     print ' i: {} with similarity : {}'.format(i, self.getSimilarityWithAnswer(_lst_answer_img[i]))
        #     _tmp_dict[i] = self.getSimilarityWithAnswer(_lst_answer_img[i])
        # answer = self.getMaxSimilarity(_tmp_dict) + 1
        # print 'Found answer: {}'.format(answer )
        # return answer 

    #Sovle E-05
    def getBinaryTransformByUnionForCol(self):
        _lst_answer = []
        _colOne = self._ravensImage.getColOne()
        _colTwo = self._ravensImage.getColTwo()
        _lst_answer_img = self._ravensImage.getListAnswerImg()
        _transform_dict_col_one = self.getBinarySimilarityScore(_colOne)
        print '++++++++++++++++++++++++++++++++++++++++++++'
        _transform_dict_col_two = self.getBinarySimilarityScore(_colTwo)
        _transform_type_one = self.getBinaryTransformType(_transform_dict_col_one)
        print '=========================================='
        _transform_type_two = self.getBinaryTransformType(_transform_dict_col_two)
        for i in  range(0, len(_lst_answer_img)):
            _col_3 = self._ravensImage.getColThree()
            _col_3.append(_lst_answer_img[i])
            _img = self.applyBinaryTransform(_transform_type_one, _col_3[0], _col_3[1])
            if self.getSimilarityIdentity(_img,  _col_3[2]):
                _lst_answer.append(self.getSimilarityIdentity(_img,  _col_3[2]))
        _max_value = max(_lst_answer)
        _max_index = _lst_answer.index(_max_value)
        print 'Found answer: {}'.format(_max_index + 1)
        return _max_index + 1

    #Solve E-01 to E-03
    def getBinaryTransformByUnionA_C_B(self):
        _lst_answer = []
        _rowOne = self._ravensImage.getRowOne()
        _rowTwo = self._ravensImage.getRowTwo()
        _lst_answer_img = self._ravensImage.getListAnswerImg()
        _transform_dict_row_one = self.getBinarySimilarityScore(_rowOne)
        _transform_dict_row_two = self.getBinarySimilarityScore(_rowTwo)
        _transform_type_one = self.getBinaryTransformType(_transform_dict_row_one)
        _transform_type_two = self.getBinaryTransformType(_transform_dict_row_two)
        for i in  range(0, len(_lst_answer_img)):
            _row_3 = self._ravensImage.getRowThree()
            _row_3.append(_lst_answer_img[i])
            _img = self.applyBinaryTransform(_transform_type_one, _row_3[0], _row_3[1])
            if self.getSimilarityIdentity(_img,  _row_3[2]):
                _lst_answer.append(self.getSimilarityIdentity(_img,  _row_3[2]))
        _max_value = max(_lst_answer)
        _max_index = _lst_answer.index(_max_value)
        print 'Found answer: {}'.format(_max_index + 1)
        return _max_index + 1

    #TODO: Getting PATTERN if pixel A > Pixel B, then make decision

    # Collected all cases can be solve by pixels
    def getBinaryTransformByPixel(self):
        return getTransformBySubtractionA_B_equal_C()


    # For E:04 : A-B = C
    def getTransformBySubtractionA_B_equal_C(self):
        _lst_answer = []
        _rowOne = self._ravensImage.getRowOne()
        _rowTwo = self._ravensImage.getRowTwo()
        _lst_answer_img = self._ravensImage.getListAnswerImg()
        for i in  range(0, len(_lst_answer_img)):
            _row_3 = self._ravensImage.getRowThree()
            _row_3.append(_lst_answer_img[i])
            if self.isSubtractPixelEqual(_rowOne) and self.isSubtractPixelEqual(_rowTwo):
                _row_3.append(_lst_answer_img[i])
                _lst_answer.append(abs(self.getPixelsSubtraction(_row_3[0], _row_3[1]) - self._ravensImage.getBlackPixels(_lst_answer_img[i])))
        _min_value = min(_lst_answer)
        _min_list = self.searchIndexWithValue(_min_value, _lst_answer) #[1,7]
        _tmp_dict = {}
        for i in _min_list:
            _tmp_dict[i] = self.getSimilarityWithAnswer(_lst_answer_img[i])
        answer = self.getMaxSimilarity(_tmp_dict) + 1
        print 'Found answer: {}'.format(answer )
        return answer 

    # used by getBinaryTransformByPixel
    def getSimilarityWithAnswer(self, _img):
        _dark_pixel_G = self._ravensImage.getBlackPixels(self._ravensImage.getImageG())
        _dark_pixel_H = self._ravensImage.getBlackPixels(self._ravensImage.getImageH())
        if _dark_pixel_G > _dark_pixel_H:
            return self._ravensImage.getEuclideanDistance(self._ravensImage.getImageG(), _img)
        else:
            return self._ravensImage.getEuclideanDistance(self._ravensImage.getImageH(), _img)

    #Return index which has value
    def searchIndexWithValue(self, _value, _lst_answer):
        _tmp = []
        count = 0
        while count < len(_lst_answer):
            if _lst_answer[count] == _value:
                _tmp.append(count)
            count += 1
        return _tmp


    def getPixelsSubtraction(self, _imgA, _imgB):
        return abs(self._ravensImage.getBlackPixels(_imgA) - self._ravensImage.getBlackPixels(_imgB))

    # Pixel A - Pixel B = Pixel C
    def isSubtractPixelEqual(self, _row):
        return True if self.getPixelsSubtraction(_row[0], _row[1]) == self._ravensImage.getBlackPixels(_row[2]) else False
